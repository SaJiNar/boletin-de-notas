import java.util.Scanner;
public class Main {
    public static void main(String[] args) {

        Scanner escaner = new Scanner(System.in);

        //Metodo para enlistar los alumnos
        Alumnos.listaAlumnos();

        System.out.println("Antes de empezar, revise su número de usuario antes de continuar");
        //System.out.println(ID+". "+nombre+"\n");
        System.out.println("1. Africa Reinoso" + '\n' + "2. Alejandro Cazalla" + '\n' + "3. Alejandro Gallego" +
                '\n' + "4. Alfonso Anillo" + '\n' + "5. Ana Maria Visiga" + '\n' + "6. Anastasia Fernández" + '\n' +
                "7. Andres Tenllado" + '\n' + "8. Beatriz Bello" + '\n' + "9. Claudia Marilyn Ferreira" + '\n' +
                "10. Dolores Rechi " + '\n' + "11. Eduardo Rojas" + '\n' + "12. Emilio Senabre" + '\n' +
                "13. Ignacio Lorca" + '\n' + "14. Jhon Ferney Aristizabal" + '\n' + "15. Juan García" + '\n' +
                "16. Maria Isabel Ortega" + '\n' + "17. Natanael Alberto Eusebio" + '\n' +  "18. Ramon Jesus Gómez"
                + '\n' + "19. Ruddy Eddi Perez" + '\n' + "20. Sara Jiménez" + '\n' + "21. Sergio Carrera"
                + '\n' + "22. Sergio Fernandez" + '\n' + "23. Sergio Lopez" + '\n' + "Seleccione a continuación el usuario:");

        //Seleccion del alumno
        int eleccion = escaner.nextInt();

        while (eleccion > 23 || eleccion <= 0) {
            System.out.println("No se encuentra el usuario en nuestra lista, por favor, introduzca de nuevo su número de usuario");
            eleccion = escaner.nextInt();
        }

        //Asignar valor al alumno
        Alumnos alumno;

        switch (eleccion) {
            case 1:
                alumno = Alumnos.JN2001;
                break;
            case 2:
                alumno = Alumnos.JN2002;
                break;
            case 3:
                alumno = Alumnos.JN2003;
                break;
            case 4:
                alumno = Alumnos.JN2004;
                break;
            case 5:
                alumno = Alumnos.JN2005;
                break;
            case 6:
                alumno = Alumnos.JN2006;
                break;
            case 7:
                alumno = Alumnos.JN2007;
                break;
            case 8:
                alumno = Alumnos.JN2008;
                break;
            case 9:
                alumno = Alumnos.JN2009;
                break;
            case 10:
                alumno = Alumnos.JN2010;
                break;
            case 11:
                alumno = Alumnos.JN2011;
                break;
            case 12:
                alumno = Alumnos.JN2012;
                break;
            case 13:
                alumno = Alumnos.JN2013;
                break;
            case 14:
                alumno = Alumnos.JN2014;
                break;
            case 15:
                alumno = Alumnos.JN2015;
                break;
            case 16:
                alumno = Alumnos.JN2016;
                break;
            case 17:
                alumno = Alumnos.JN2017;
                break;
            case 18:
                alumno = Alumnos.JN2018;
                break;
            case 19:
                alumno = Alumnos.JN2019;
                break;
            case 20:
                alumno = Alumnos.JN2020;
                break;
            case 21:
                alumno = Alumnos.JN2021;
                break;
            case 22:
                alumno = Alumnos.JN2022;
                break;
            case 23:
                alumno = Alumnos.JN2023;
                break;
            default:
                alumno = Alumnos.JN2011;
                break;

        }
//Asignacion de notas

        double[] notas = new double[12];

        System.out.println("Nos alegramos de verle " + alumno.getNombre() + ", por favor, introduzca sus notas:");
        System.out.println("Introduzca la nota de " + Materias.CJ001.getDescripcion() + ": ");
        notas[0] = escaner.nextDouble();
        System.out.println("Introduzca la nota de " + Materias.CJ002.getDescripcion() + ": ");
        notas[1] = escaner.nextDouble();
        System.out.println("Introduzca la nota de " + Materias.CJ003.getDescripcion() + ": ");
        notas[2] = escaner.nextDouble();
        System.out.println("Introduzca la nota de " + Materias.CJ004.getDescripcion() + ": ");
        notas[3] = escaner.nextDouble();
        System.out.println("Introduzca la nota de " + Materias.CJ005.getDescripcion() + ": ");
        notas[4] = escaner.nextDouble();
        System.out.println("Introduzca la nota de " + Materias.CJ006.getDescripcion() + ": ");
        notas[5] = escaner.nextDouble();
        System.out.println("Introduzca la nota de " + Materias.CJ007.getDescripcion() + ": ");
        notas[6] = escaner.nextDouble();
        System.out.println("Introduzca la nota de " + Materias.CJ008.getDescripcion() + ": ");
        notas[7] = escaner.nextDouble();
        System.out.println("Introduzca la nota de " + Materias.CJ009.getDescripcion() + ": ");
        notas[8] = escaner.nextDouble();
        System.out.println("Introduzca la nota de " + Materias.CJ010.getDescripcion() + ": ");
        notas[9] = escaner.nextDouble();
        System.out.println("Introduzca la nota de " + Materias.CJ011.getDescripcion() + ": ");
        notas[10] = escaner.nextDouble();
        System.out.println("Introduzca la nota de " + Materias.CJ012.getDescripcion() + ": ");
        notas[11] = escaner.nextDouble();

        //Creacion del boletin
        Boletin boletin = new Boletin(alumno, notas[0], notas[1], notas[2], notas[3], notas[4], notas[5], notas[6], notas[7], notas[8], notas[9], notas[10], notas[11]);

        //Imprimir el boletin
        boletin.imprimeBoletin();
    }
}
