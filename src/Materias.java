public enum Materias {
    CJ001(1,"Java Básico","Eduardo Corral"),
    CJ002(2,"POO","Eduardo Corral"),
    CJ003(3,"Bases de Datos","Carlos Ponce de León"),
    CJ004(4,"Java Intermedio","Eduardo Corral"),
    CJ005(5,"Control Versiones","Carlos Ponce de León"),
    CJ006(6,"Pruebas Unitarias","Diego Avendano"),
    CJ007(7,"HTML/CSS","Diego Avendano"),
    CJ008(8,"Spring Boot","José Manuel Aroca"),
    CJ009(9,"Microservicios","José Manuel Aroca"),
    CJ010(10,"Presentaciones","María Calvo"),
    CJ011(11,"Soft Skills","Sonsóles Moralejo"),
    CJ012(12,"Empleo","María Teresa García");

    private int ID;
    private String descripcion;
    private String docente;

    Materias(int ID, String descripcion, String docente) {
        this.ID = ID;
        this.descripcion = descripcion;
        this.docente = docente;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public String getDocente() {
        return docente;
    }
}